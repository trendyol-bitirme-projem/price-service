package com.trendyol.priceservice.repository;

import com.trendyol.priceservice.entity.PriceEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface PriceRepository extends MongoRepository<PriceEntity, Integer> {
    Optional<PriceEntity> findByProductIdAndStockId(Integer productId, String stockId);
    void deleteByProductIdAndStockId(Integer productId, String stockId);
}

