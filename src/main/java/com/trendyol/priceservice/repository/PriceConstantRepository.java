package com.trendyol.priceservice.repository;

import com.trendyol.priceservice.entity.PriceConstantEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PriceConstantRepository extends MongoRepository<PriceConstantEntity, Integer> {
}
