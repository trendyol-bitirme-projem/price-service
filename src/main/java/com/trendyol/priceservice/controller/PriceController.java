package com.trendyol.priceservice.controller;

import com.trendyol.priceservice.entity.PriceEntity;
import com.trendyol.priceservice.model.Price;
import com.trendyol.priceservice.model.PriceQuery;
import com.trendyol.priceservice.service.PriceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/price")
@RequiredArgsConstructor
public class PriceController {

    public final PriceService priceService;

    @PostMapping
    public PriceEntity savePrice(@RequestBody PriceEntity priceEntity) throws Exception {
        return priceService.savePrice(priceEntity);
    }

    @PutMapping
    public PriceEntity updatePrice(@RequestBody PriceEntity priceEntity) throws Exception {
        return priceService.updatePrice(priceEntity);
    }

    @DeleteMapping
    public ResponseEntity<?> deletePrice(@RequestBody PriceEntity priceEntity) {
        priceService.deletePrice(priceEntity);
        return ResponseEntity.status(HttpStatus.OK).body("Price is deleted.");
    }

    @PostMapping("/search")
    public Price getPrice(@RequestBody List<PriceQuery> priceQueryList) throws IOException {
        return priceService.getPrice(priceQueryList);
    }
}
