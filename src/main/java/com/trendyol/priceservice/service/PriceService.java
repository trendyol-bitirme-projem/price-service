package com.trendyol.priceservice.service;

import com.trendyol.priceservice.entity.PriceConstantEntity;
import com.trendyol.priceservice.entity.PriceEntity;
import com.trendyol.priceservice.model.*;
import com.trendyol.priceservice.repository.PriceConstantRepository;
import com.trendyol.priceservice.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class PriceService {

    public final PriceRepository priceRepository;
    public final PriceConstantRepository priceConstantRepository;

    public PriceEntity savePrice(PriceEntity priceEntity) throws Exception {
        Optional<PriceEntity> existPrice = priceRepository.findByProductIdAndStockId(priceEntity.getProductId(), priceEntity.getStockId());
        if(existPrice.isEmpty()){
            priceEntity.setId(Objects.hash(UUID.randomUUID().toString()));
            return priceRepository.save(priceEntity);
        }else{
            throw new Exception("Please use put method to update price");
        }
    }

    public PriceEntity updatePrice(PriceEntity priceEntity) throws Exception {
        Optional<PriceEntity> existPrice = priceRepository.findByProductIdAndStockId(priceEntity.getProductId(), priceEntity.getStockId());
        if(existPrice.isPresent()){
            return priceRepository.save(priceEntity);
        }else{
            throw new Exception("Please use post method to save price");
        }
    }

    public void deletePrice(PriceEntity priceEntity){
        priceRepository.deleteByProductIdAndStockId(priceEntity.getProductId(), priceEntity.getStockId());
    }

    public Price getPrice(List<PriceQuery> priceQueryList){
        PriceConstantEntity priceConstantEntity = priceConstantRepository.findById(1).orElse(null);
        Price price = new Price();
        price.setCurrency(Currency.TL);
        for(PriceQuery priceQuery : priceQueryList){
            Optional<PriceEntity> _priceEntity = priceRepository.findByProductIdAndStockId(priceQuery.getProductId(), priceQuery.getStockId());
            if(_priceEntity.isPresent()){
                PriceEntity priceEntity = _priceEntity.get();
                ProductPrice productPrice = new ProductPrice();
                productPrice.setProductId(priceEntity.getProductId());
                productPrice.setStockId(priceEntity.getStockId());
                productPrice.setQuantity(priceQuery.getQuantity());
                productPrice.setPrice(priceEntity.getPrice() * priceQuery.getQuantity());
                productPrice.setCurrency(priceEntity.getCurrency());
                setTax(productPrice, priceConstantEntity);
                productPrice.setTotal(productPrice.getPrice() + productPrice.getTax());
                price.getProductPrices().add(productPrice);
                price.setSubTotal(price.getSubTotal() + (productPrice.getPrice()*productPrice.getCurrency().getValue()));
                price.setTotalTax(price.getTotalTax() + (productPrice.getTax() * productPrice.getCurrency().getValue()));
            }
        }
        if(price.getSubTotal()>priceConstantEntity.getFreeCargoMinPrice()){
            price.setCargo(0D);
        }else{
            price.setCargo(priceConstantEntity.getCargoPrice());
        }
        price.setTotal(price.getSubTotal()+price.getCargo()+price.getTotalTax());
        return price;
    }

    public void setTax(ProductPrice productPrice, PriceConstantEntity priceConstantEntity){
        double price = (productPrice.getPrice()/productPrice.getQuantity()) * productPrice.getCurrency().getValue();
        for(TaxPrice taxPrice : priceConstantEntity.getTaxPrices()){
            if(price>=taxPrice.getStart() && price<=taxPrice.getEnd()){
                productPrice.setTax((taxPrice.getPrice() / productPrice.getCurrency().getValue())*productPrice.getQuantity());
            }
        }
    }
}
