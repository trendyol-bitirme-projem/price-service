package com.trendyol.priceservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceQuery {
    private Integer productId;
    private String stockId;
    private Integer quantity;
}
