package com.trendyol.priceservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductPrice {
    private Integer id;
    private Integer productId;
    private String stockId;
    private Integer quantity;
    private Double price;
    private Currency currency;
    private Double tax;
    private Double total;
}
