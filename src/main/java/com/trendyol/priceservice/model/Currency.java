package com.trendyol.priceservice.model;

import lombok.Getter;

@Getter
public enum Currency {
    TL(1),
    DOLLAR(8),
    EURO(10);

    private Integer value;

    Currency(Integer value){
        this.value = value;
    }
}
