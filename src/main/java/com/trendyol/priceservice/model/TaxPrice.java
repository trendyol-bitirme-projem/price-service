package com.trendyol.priceservice.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaxPrice {
    private Integer start;
    private Integer end;
    private Double price;
}
