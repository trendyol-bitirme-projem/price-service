package com.trendyol.priceservice.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class Price {
    private List<ProductPrice> productPrices = new ArrayList<>();
    private double subTotal;
    private double cargo;
    private double totalTax;
    private double total;
    private Currency currency;
}
