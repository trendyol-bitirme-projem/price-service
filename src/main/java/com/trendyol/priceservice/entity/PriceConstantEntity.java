package com.trendyol.priceservice.entity;

import com.trendyol.priceservice.model.TaxPrice;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Document("price-constant")
public class PriceConstantEntity {
    @Id
    private Integer id;
    private Double cargoPrice;
    private Double freeCargoMinPrice;
    private List<TaxPrice> taxPrices = new ArrayList<>();
}
