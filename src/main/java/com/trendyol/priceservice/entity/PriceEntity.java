package com.trendyol.priceservice.entity;

import com.trendyol.priceservice.model.Currency;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document("prices")
public class PriceEntity {
    @Id
    private Integer id;
    private Integer productId;
    private String stockId;
    private Double price;
    private Currency currency;
}
